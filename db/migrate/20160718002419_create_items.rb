class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name
      t.text :description
      t.decimal :price
      t.string :category
      t.integer :stock
      t.string :img_url

      t.timestamps null: false
    end
  end
end
