class CartController < ApplicationController
  
  before_action :authenticate_user!, except: [:index]
  
  def add
    id = params[:id] 
    cart[id] ? cart[id] = cart[id] + 1 : cart[id] = 1 
    redirect_to :action => :index
  end
    
  def clearCart
    session[:cart] = nil
    redirect_to :action => :index
  end
  
  def index
    #if there is a cart pass it to the page for display else pass an empty value
    if session[:cart] then
      @cart = session[:cart]
    else
      @cart = {}
    end
  end
end
